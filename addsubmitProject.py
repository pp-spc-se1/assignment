from dbconnect import *
import os,subprocess
colNames = []
projectFileName = []
groupId = []
def readFile(filePath):
    fp = open(filePath,'rb')
    imgData = fp.read()
    return psycopg2.Binary(imgData)

def validation(record):
    if len(colNames) > len(record):
        print 'ERROR: null value in column'
        print 'DETAIL: Failing row contains',record
        return False

    elif len(colNames) < len(record):
        print 'ERROR: row contain more than target columns',record
        return False
    else:
        for value in record:
            if value.isdigit():
                print 'ERROR: invalid input syntax ',record
                return False
            else:
                return True


def addSubmitProjectRecordf(fileName):
    fp = open(fileName,'r')
    lines = fp.readlines()
    
    submitProjectRecord = []
    map(lambda line : submitProjectRecord.append(line.split(',')),lines)
    cur.execute("Select * FROM group_project_submissions LIMIT 0")   # select all columns name 
    map(lambda colname: colNames.append(colname[0])  , cur.description)
    
    cur.execute("select group_id from group_project_submissions ")
    map(lambda colvalue: groupId.append(colvalue[0]) , cur.fetchall() )
    
    
    for record in submitProjectRecord:
        grpid = record[0]
        if grpid not in groupId:
            record[2] = readFile(record[2])
            record[-1] = record[-1].strip('\n')
            print record
            if (validation(record) == True):
                try:
                    cur.execute("insert into group_project_submissions values (%s,%s,%s,%s,%s,%s,null)",(record[0],record[1],record[2],record[3],record[4],record[5],))
		    print 'successfully added records'
                except psycopg2.DatabaseError, e:
                    print 'Error %s' % e    
            
        conn.commit()

    #getProjectFileP()
            
if __name__=='__main__':
    args = sys.argv[1:]
    if len(args) == 2:
        cmd = args[0]
        if cmd == 'addSubmitProject':
            addSubmitProjectRecordf(args[1])
        else:
            print 'ERROR : Command name should be addSubmitProject'
    else:
        print 'Usage : python addsubmitProject.py addSubmitProject groupProjectSubDetails.txt'
