from dbconnect import *
colNames = []

def validation(record):
    if len(colNames) > len(record):
        print 'ERROR: null value in column'
        print 'DETAIL: Failing row contains',record
        return False

    elif len(colNames) < len(record):
        print 'ERROR: row contain more than target columns',record
        return False
    else:
        for value in record:
            if value.isdigit():
                print 'ERROR: invalid input syntax ',record
                return False
            else:
                return True


def addGroupRecordf(fileName):
    fp = open(fileName,'r')
    lines = fp.readlines()
    groupRecord = []
    map(lambda line : groupRecord.append(line.split(',')),lines)
    cur.execute("Select * FROM groups LIMIT 0")   # select all columns name 
    map(lambda colname: colNames.append(colname[0])  , cur.description)
    
    for record in groupRecord:
        record[-1] = record[-1].strip('\n')
        if (validation(record) == True):
            try:
                cur.execute("insert into groups values (%s,%s)",(record[0],record[1]))
            except psycopg2.DatabaseError, e:
                print 'Error %s' % e    
            
        conn.commit()
    
if __name__=='__main__':
    
    args = sys.argv[1:]
    if len(args)==2:
        cmd = args[0]
        if cmd == 'addGroups':
            addGroupRecordf(args[1])
        else:
            print 'ERROR : Command name should be -> addGroups'
    else:
        print 'Usage : python addGroup.py addGroups groupDetails.txt'
