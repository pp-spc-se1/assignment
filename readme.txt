Name     : Farman Ali
Roll No. : 13103

Add group's details into groups table(database).
type the command : python addGroup.py addGroups groupDetails.txt

Now, add some group's project tar file into the group_project_submissions table(database).
type the command : python addsubmitProject.py  addSubmitProject groupProjectSubDetails.txt

Now, run handler.py 
	this program execute two processes
		 1) getSubmitProjectFile.py (this process create temp directory in your current directory and copy all the tar files from 			database to temp directory and continuously check for new entry in database if get any then copy that tar file from 			database to temp directory)

		 2) tarFileProcess.py (this process read all the tar files one by one from temp directory and create a directory with name of 			that tar file and move that tar file into this directoty.Untar that tar file in that directory and check the untar file name 			is same as the tar file name.If the name is not same then delete that directory by the tarFileProcess.
		
		3) readmeProcess.py ( this process take untar file (ie accepted by tarFileProcess  ) and check readme file exist.If readme 			file not exist then delete that directory by the readmeProcess.)
	
	And these processes continuously running.
