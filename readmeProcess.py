import os,time,shutil,re
currentDirPath = os.getcwd()
tempDirPath = currentDirPath +  '/temp/'
delay = 10
def isReadmeFileExist(files):
    for eachFile in files:
        try:
            re.match(r'^readme(.te?xt)?$',eachFile,re.IGNORECASE).group()
            return True
        except:
            continue
def readmeProcess():
    while True:
        if os.path.exists(tempDirPath):
            dirList = filter(lambda x:  os.path.isdir(tempDirPath+x)  ,os.listdir(tempDirPath))
            if dirList!=[]:
                fp1 = open('dirName.txt','a+')
                lst = fp1.read().split()
                for eachDir in dirList:
                    if eachDir not in lst:
                        lst.append(eachDir)
                        fp1.write(eachDir)
                        fp1.write('\n')
                        eachDirPath = tempDirPath+eachDir+'/'
                        dirNames = os.listdir(eachDirPath)
                        files = os.listdir(eachDirPath + filter(lambda x: os.path.isdir(eachDirPath+x) ,dirNames)[0])
                        #print files
                        if files!=[]:
                            if  isReadmeFileExist(files):
                                print 'accepted by readmeProcess ',eachDir
                            else:
                                print 'rejected by readmeProcess ',eachDir
                                #print eachDirPath
                                shutil.rmtree(eachDirPath)
                fp1.close()
        time.sleep(delay)
if __name__=='__main__':
    readmeProcess()
