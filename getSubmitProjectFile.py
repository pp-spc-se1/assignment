from dbconnect import *
import os,time
delay = 10
def writeFile(data,pfileName):
    fp = open(pfileName,'wb')
    fp.write(data)
    fp.close()
def getProjectFileP():
    currentDirPath = os.getcwd()
    while True:
        finalDir = os.path.join(currentDirPath, 'temp/')
        if not os.path.exists(finalDir):
            os.makedirs(finalDir)
        fp = open('tarfileName.txt','a+')
        lst = fp.read().split()
        projectFileName = []
        cur.execute("select project_file_name from group_project_submissions ")
        map(lambda colvalue: projectFileName.append(colvalue[0]) , cur.fetchall() )
        cur.execute("select project_file from group_project_submissions ")
        data = cur.fetchall()
        for i in range(len(data)):
            if projectFileName[i] not in lst:
                lst.append(projectFileName[i])
                fp.write(projectFileName[i])
                fp.write('\n')
                data1 = data[i][0]
                fileName = finalDir  + projectFileName[i]
                writeFile(data1,fileName)
        fp.close()    
        time.sleep(delay)
if __name__=='__main__':
    getProjectFileP()
