drop database pp_spc_se1 ;
drop table batches ;
drop table student_groups ;
drop table group_projects ;
drop table group_project_submissions ;
drop table students ;
drop table groups ;
drop table projects ;
drop table subjects ;

create database pp_spc_se1;

create table students (
	stu_roll_no int primary key,
	stu_name varchar(30) NOT NULL,
	stu_batch_name varchar(20) NOT NULL,
	stu_email varchar(30) NOT NULL,
	stu_mobile_no char(10) NOT NULL,
	stu_photo_path_name text NOT NULL,
	image BYTEA NOT NULL
);


create table subjects (
	sub_code char(10) primary key,
	sub_name varchar(50) NOT NULL,
	sub_syllabus text NOT NULL
);

create table batches (
	batch_id varchar(10) primary key,
	batch_year int NOT NULL
);
create table groups (
	group_id varchar(10) primary key,
	group_name varchar(20) NOT NULL
);

create table projects (
	project_id varchar(10) primary key,
	project_name varchar(50) NOT NULL, 
	project_description text NOT NULL,
	project_deadline timestamp NOT NULL,
	project_assign_date date NOT NULL
);

create  table group_projects (
	group_id varchar(10) primary key references groups(group_id),
	project_id varchar(10) references projects(project_id) 
);


create table student_groups(
	group_id varchar(10) references groups(group_id),
	stu_roll_no int primary key references students(stu_roll_no)
);
create table reject_reasons(
	reason_id char(50) primary key,
	description text
);

create table group_project_submissions(
	group_id varchar(10) primary key  references groups(group_id),
	submission_date timestamp NOT NULL,
	project_file BYTEA NOT NULL,	
	project_file_name text NOT NULL,
	remarks text NOT NULL,
	accepted char(10),
	reason char(50) references reject_reasons(reason_id) 
);


