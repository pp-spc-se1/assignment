import os,shutil,time,tarfile
currentDirPath = os.getcwd()
tempDirPath = currentDirPath + '/temp/'
delay = 10
def tarFileProcess():
    while True:
        if os.path.exists(tempDirPath):
            fileList = os.listdir(tempDirPath) # read all the files from temp directory
            for tarName in fileList:
                if tarName.endswith('tar.gz'):
                    lst = tarName.replace('.tar.gz','')
                    dirPath = tempDirPath + lst + '/'
                    if not os.path.exists(dirPath):
                        os.makedirs(dirPath)
                    
                    shutil.move(tempDirPath+tarName,dirPath)
                
                    tar = tarfile.open(dirPath+tarName,'r:gz')
                    tar.extractall(dirPath)
                    tar.close()
                    for name in  os.listdir(dirPath):
                        if os.path.isdir(os.path.join(dirPath,name)):
                            if name!=lst:
                                print 'rejected by tarProcess ',tarName
                                shutil.rmtree(dirPath)
			    else:
				print 'accepted by tarProcess ',tarName
        time.sleep(delay)
if __name__=='__main__':
    tarFileProcess()
